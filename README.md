# Introduction to Jest

This a simple exercise with multiple examples to demonstrate how Jest JS can be used for testing.

## Prerequisites

- NodeJS
- Npm

## Local Installation

Clone the project

```sh
git clone https://gitlab.com/ayushsri/intro-to-jestjs.git
cd intro-to-jestjs
```

Installing node modules (dependencies)

```sh
npm install
```
Running Test Server

```sh
json-server --watch users.json
```

Testing using jest

```sh
npm test
```
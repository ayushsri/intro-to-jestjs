// Jest grouping tests

// In Jest, tests are grouped into units with describe().
// It creates a block that groups together several related tests.

const isPalindrome = (string) => string == string.split('').reverse().join('');

const isAnagram = (w1, w2) => {

    const regularize = (word) => {
        return word.toLowerCase().split('').sort().join('').trim();
    }

    return regularize(w1) === regularize(w2);
}

module.exports = {isPalindrome, isAnagram};

// We have string-utils.js module with two functions: isPalindrome() and isAnagram().
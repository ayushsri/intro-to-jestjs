// Jest testing Axios

// There is some fake data for the JSON server in users.json

const axios = require('axios');

class Users {

     static async all() {
        let res = await axios.get('http://localhost:3000/users');
        return res;
      }
}

module.exports = Users;

// The users.js module retrieves data with axios. We will test this module.

// START SERVER -- COMMAND IN startSever.js
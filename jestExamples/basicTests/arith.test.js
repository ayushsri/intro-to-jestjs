// Testing arithmetic functions with Jest

const { add, mul, sub, div } = require('./arith');

// We test the add() method with test() function.
// The first parameter is the name of the test, the second parameter is the function to be run.
// We are testing that the add() function returns correct answer for sample data.

test('2 + 3 = 5', () => {
  expect(add(2, 3)).toBe(5);
});

test('3 * 4 = 12', () => {
  expect(mul(3, 4)).toBe(12);
});

test('5 - 6 = -1', () => {
  expect(sub(5, 6)).toBe(-1);
});

test('8 / 4 = 2', () => {
  expect(div(8, 4)).toBe(2);
});

// You can also test cases that should not be true i.e any unexpected result
// For example 8/4 should equal to 2 but not be equal to something like 10

test('8 / 4 != 10', () => {
  expect(div(8, 4)).not.toBe(10);
});

test('5 * 4 != 10', () => {
  expect(mul(5, 4)).not.toBe(10);
});